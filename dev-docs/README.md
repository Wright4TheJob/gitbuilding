# GitBuilding DevDocs

This is auto generated with `dev_docs.py` it is not complete
documentation for development. It is just a way to visualise the code structure.

## Module import graph
![](gitbuilding.svg)

## Modules



### Sub-Module: gitbuilding.handler

>  This submodule deals with the logging handler that GitBuilding uses to pretty print warnings into the terminal and to forward warnings to the server 

#### Class: gitbuilding.handler.GBHandler

>  A child class of logging.Handler. This class which handles logging records for GitBuilding it logs to std.out in colour and saves a warnings to an internal log that that can then be accessed by the server. 

### Sub-Module: gitbuilding.output

>  This module handles all the Builder classes that produce outputs. Currently the outputs are * HTML provided by StaticSiteBuilder * Markdown provided by MarkdownBuilder to make a custom Builder you can inherit from the Builder class. 

#### Class: gitbuilding.output.Builder

>  Base class for Builder classes. Do not use this class. 

#### Class: gitbuilding.output.MarkdownBuilder

>  Class to build a markdown directory from a BuildUp directory. 

#### Class: gitbuilding.output.PdfBuilder

>  Class to build a static website from a BuildUp directory. 

#### Class: gitbuilding.output.StaticSiteBuilder

>  Class to build a static website from a BuildUp directory. 

### Sub-Module: gitbuilding.native_file_operations

>  This handles native file operations such as checking if files exists on disk and writing to them. It is not much more than a simple wrapper around some os.* functions.  The purpose of this module is to ensure that all paths in other modules can be trated as posix paths. This is important otherwise you need to track whether a is in posix style for web url use or is in native style. 

### Sub-Module: gitbuilding.generate_ci

>  This module automatically creates CI configurations to serve GitBuilding documentation for GitLab, GitHub actions, and Netlify. 

### Sub-Module: gitbuilding.example

> This module is for making an simple example GitBuilding project.  It is used when you run `gitbuilding new`

### Sub-Module: gitbuilding.render

>  This contains GBRenderer, the class responsible for rendering processed markdown into HTML It also contains the URLRules GitBuilding uses for HTML and some other helper functions. 

#### Class: gitbuilding.render.GBRenderer

>  This class is the renderer for GitBuilding HTML 

#### Class: gitbuilding.render.URLRulesHTML

>  The BuildUp URLRules used in GitBuilding for both the server and the static HTML. This is a child-class of buildup.URLRules with functions to strip off '.md' file extensions, rerouted stl links (for parts only) to markdown pages, and to replace empty links with "missing". 

#### Class: gitbuilding.render.URLRulesPDF

>  The BuildUp URLRules used in GitBuilding PDF generation (via single pageHTML). This is a child-class of buildup.URLRules with functions to strip off '.md' file 

### Sub-Module: gitbuilding.previewers

>  This module provides the default previewers for GitBuilding. Previewers scan image-style markdown links for filtypes and links that can be automatically previewed 

#### Class: gitbuilding.previewers.PDFPreviewer

>  Build-up previewer that will preview PDF files 

#### Class: gitbuilding.previewers.ThreeDimPreviewer

>  Build-up previewer that will preview 3D models with GB3D 

#### Class: gitbuilding.previewers.YouTubePreviewer

>  Build-up previewer that will embed YouTube links. 

### Sub-Module: gitbuilding.server

>  This module constains the flask server for viewing the documentation and for live editing. The server is lunched with `gitbuilding serve` and runs on port 6178. 

#### Class: gitbuilding.server.DevServer

>  Child class of GBServer, this server allows hot-reloading of live-editor for development. 

#### Class: gitbuilding.server.DroppedFiles

>  Pretty simple class for handling the files dropped into the editor. This could be handled with a list of dictionaries but the syntax for checking and finding the correct file gets really ugly. 

#### Class: gitbuilding.server.GBServer

>  GBServer is the GitBuilding server it is a child class of flask.Flask. It can be used to provide a preview of the documentation and to serve the live editor. 

#### Class: gitbuilding.server.GBWebPath

>  GBWebPath is a class that parses paths from the server and can return either the corresponding local path or the correct special path. 

#### Class: gitbuilding.server.ServerAlreadyRunningError

>  Custom exception for if the GitBuilding server is already running. 

### Sub-Module: gitbuilding.utilities

>  A number of miscellaneous functions 

### Sub-Module: gitbuilding.config

>  This module deals with loading and parsing the configuration file the main schema GBConfigSchema is subclassed from buildup.ConfigSchema. All of extra GitBuilding specific options are added. The helper functions for loading also deal with removing invalid configuration options and logging a warning. 

#### Class: gitbuilding.config.GBConfigSchema

>  This is a subclass of buildup.ConfigSchema it is used to add all the extra configuration options to the base set of options used by BuildUp. 

### Sub-Module: gitbuilding.__main__

>  The entrypoint of GitBuilding it is run when `gitbuilding` is run from the commandline. 

#### Class: gitbuilding.__main__.GBParser

>  The GitBuilding commandline (argparse) parser, it has a number of sub-parsers for the sub-commands such as `build` or `serve` 

### Sub-Module: gitbuilding.buildup.unit

>  This sub-module deals with units and unit conversion. Unlike other more advanced unit modules like `pint` this module only deals with physical quantities of length, area, volume, and mass. It is built from the ground up to group units with similar units for preffered conversions. UnitGroups are then related in a tree like definition structure so conversions (hopefully) automatically choose an appropriate unit. 

#### Class: gitbuilding.buildup.unit.Unit

>  This class defined a list of possible names for a unit. It containts no other information. The first name in the definition is the preffered name. This class registers as equal to any string equal to one of the unit's names. This means strings can always be used in place of a Unit object when using the funtions in UnitLibrary and UnitGroup. 

#### Class: gitbuilding.buildup.unit.UnitGroup

>  A UnitGroup is a group of similar units such as a uL, mL, L, and the scalins between them. The inputs are, the base unit for the group, a list of tuples of the form (Unit, scale) where scale is the scaling from the unit to the base unit as an int, float, or Fraction Optional inputs parent_group and parent_scale should either both be set or neither should be set. The parent_group is another group of units of a more primary definition for the same quanity. The scale is between the base units of the two groups. 

#### Class: gitbuilding.buildup.unit.UnitLibrary

>  This class defines all Units and UnitGroups, it holds the list of all unit groups. The library can be used to check if units are convertable, the preffered unit for this conversion, and the scalings for the conversion. It can also be used to scale quantities to a more appropriate unit inside a UnitGroup. 

### Sub-Module: gitbuilding.buildup.partlist

>  This submodule contains functionality to handle lists of parts in the BuildUp documentation. 

#### Class: gitbuilding.buildup.partlist.PartList

>  PartLists are lists of Part objects. They have functions that allow them to safely add parts and to be merged. 

### Sub-Module: gitbuilding.buildup.quantity

>  This submodule containts functionality to store and manipulate quantities that can be either purely numerical, numerical with units, or purely descriptive. The Quantity object is used to store what type of quantity and to handle quantity addition. The `largest_quantity` function is used instead of `max(quantity1, quantity2)` as lagrest quantity sets a new output that is equal to neither of the inputs when the answer is ambiguous. 

#### Class: gitbuilding.buildup.quantity.Quantity

>  A quantity objects represents the quantity for a part. The quantity object is responsible for interpreting the incoming quantity string. It can hold numerical quantities with and without units, as well as descriptive quantities. Input to constructor is the quantity string from the BuildUp file. 

### Sub-Module: gitbuilding.buildup.link

>  This sub module contains the BaseLink class and its child classes Link, LibraryLink, and Image. Instead of constructing a Link or LibraryLink use `make_link`. This will return the correct object type. Image is used only for displaying images i.e. links that start with an !. For a reference to an image use `make_link`. 

#### Class: gitbuilding.buildup.link.BaseLink

>  A base class for a link. Can is used to do a number of things from completing reference style links. Translating links to be relative to different pages and generating the output FileInfo objects. Do not use it directly. Use a the child class: * Image or the function the function `make_link`. This  will assign the correct type between `Link`, and `LibraryLink`. 

#### Class: gitbuilding.buildup.link.FromStepLink

>  A child class of BaseLink for links to parts produced in previous documentation steps 

#### Class: gitbuilding.buildup.link.Image

>  A child class of Link to deal with the subtle differences of Links and Images in markdown. 

#### Class: gitbuilding.buildup.link.LibraryLink

>  A child class of BaseLink for links to parts in Libraries. It translates the from the link in the library to the final markdown page. Then other translations happen as standard. 

#### Class: gitbuilding.buildup.link.Link

>  A link to another file in the Documentation directory. See also LibraryLink. This class should always be created with `make_link` which will create the correct link type. The child class Image can be created directly with its constructor. 

#### Class: gitbuilding.buildup.link.LinkData

>  A simple data class for the BuildUp data stored with a link the input data is validated by a marshmallow schema. This class has an extra validation step to check that data isn't defined for the wrong type of link. 

#### Class: gitbuilding.buildup.link.LinkDataSchema

>  This is the schema for the extra data that can be appended on a link 

#### Class: gitbuilding.buildup.link.LinkInfo

>  A simple data class for the key information defined for a link. Ths mostly exists to aid finding coding errors which cannot be done using dictionaries. 

### Sub-Module: gitbuilding.buildup.buildup

>  The main submodule for parsing BuildUp. This constains both the BuildUpParser class and classes Links and Images. It also contains the function that parses the inline part data. 

#### Class: gitbuilding.buildup.buildup.BuildUpParser

>  This is main parser for reading the buildup. It is not really a parser, it is just about 8 or so regexs that find the BuildUp specific syntax. An object is initialised with the raw text to parse, the directory of the page the text is from so that the any relative links can be translated 

### Sub-Module: gitbuilding.buildup.basepart

>  This submodule contains functionality to handle parts in the BuildUp documentation. 

#### Class: gitbuilding.buildup.basepart.Part

>  A part is anything that can be counted in the documentation. This includes components and tools that are used (UsedParts). And other parts created in a page (CreatedPart). 

#### Class: gitbuilding.buildup.basepart.PartData

>  Part data and object that represents the YAML data that sets information about the parts that need to be purchased. This is the data specific to the item (Such as the supplier, the material specifications, etc) not the data about how it is used within the documentation (This data his held in the UsedPart class). 

#### Class: gitbuilding.buildup.basepart.PartDataSchema

>  A marshmallow schema for parsing the dara for each item in a part library 

#### Class: gitbuilding.buildup.basepart.ProductSchema

>  A simple marshmallow schema for the data for each product lists for a supplier in part data. Returns this as a dataclass 

#### Class: gitbuilding.buildup.basepart._LaxStrFeild

>  The is a subclass of field.Str that coerces float and integer objects into strings rather than warning 

#### Class: gitbuilding.buildup.basepart._ProductsField

>  _ProductsField is used by marshmallow to serialise and de-serialise the product data for a supplier listed in the part data. 

#### Class: gitbuilding.buildup.basepart._SuppliersField

>  _SuppliersField is used by marshmallow to serialise and de-serialise the data from all suppliers. 

### Sub-Module: gitbuilding.buildup.preview

>  Define html previews for file types. This will set the HTML to display a particular file type or web-link. If this is set you can display a the file or link either by using markdown image syntax. 

#### Class: gitbuilding.buildup.preview.Previewer

>  Base class for previewers this should be sub-classed for use. 

### Sub-Module: gitbuilding.buildup.libraries

>  This submodule handles buildup part libraries. It parses the contents of the library files, identifies parts in the libraries, and generates markdown pages for parts. 

#### Class: gitbuilding.buildup.libraries.Libraries

>  Class to handle all the part libraries for the documentation. This object is initialised with the file main documentation filelist. Libraries are not parsed until the first time they are used. Any YAML file is assumed to be a library any non-yaml file is not a library. This should be modified to have a defined schema to check which YAML files are part libraries. Also other files formats that support this schema should be added (JSON, TOML, XML, etc). CSV support will also be added, need a way to convert between CSV and the nested dictionary structure of the schema. 

### Sub-Module: gitbuilding.buildup.parts

>  This submodule contains functionality to count parts used or created in buildup documentation. UsedPart is a child class of Part, it is used to track the usage of parts. CreatedPart is a child class of Part, it is used to track parts that are made.  Part is defined in the basepart submodule 

#### Class: gitbuilding.buildup.parts.CreatedPart

>  This is the class items that are created in BuildUp documentation. CreatedParts are created using "output". This class isn't called "Output" as this may get confused with the output documentation. 

#### Class: gitbuilding.buildup.parts.UsedPart

>  This class represents a particular part used in the documentation such as "M3x6mm Cap Head Screws". UsedPart is a child class of Part. It handles counting the quantity of the part used, its category, and notes about usage etc. 

### Sub-Module: gitbuilding.buildup.files

>  This submodule contains the read directory function, a helper function so that the contents of a whole directory can be passed into the Documentation object. It also contains the FileInfo object. This is used to specify information about files that are input into and output from BuildUp.  Note: No other buildup module should use os. And expecially not os.path It causes confusion as it modifies paths into native paths, whereas buildup is creating static markdown for html. It expects all internal paths to be posix paths.  

#### Class: gitbuilding.buildup.files.FileInfo

>  Class files input into the BuildUp and the files in after building `path` is the path of the file relative to the root documentation directory. This must be relative and link to a file inside the root, BuildUp does not recognise files outside of root. Use `location_on_disk` to spoof the location of a file elsewhere. `dynamic_content` should be True for file that buildup can process, or any file that buildup generates. File a file that build up doesn't process the `location_on_disk` can be set, as default this is assumed to be the path. This is None if `dynamic_content` is True `content` is none if `dynamic_content` is False, else it is the contents of the buildup file. 

### Sub-Module: gitbuilding.buildup.page

>  This submodule deals with BuildUp pages. A Page object is created for each markdown (buildup) file in the documentation directory. 

#### Class: gitbuilding.buildup.page.Page

>  This class represents one BuildUp page. It can be used to: track its relation to other pages using the step_tree; to count the parts in the page; and to export a pure markdown page. 

### Sub-Module: gitbuilding.buildup.pageorder

>  This submodule constains the PageOrder class which is used to store and manipulate the paths through the buildup documentation. 

#### Class: gitbuilding.buildup.pageorder.PageOrder

>  A PageOrder is object is initialised with the step trees from every page and the landing page. It will calculate the number of paths through the documentation, these can be accessed as trees or as lists. The lists have two versions, the list of the pages, and the list for navigation, the list for navigation also include bill of materials. 

### Sub-Module: gitbuilding.buildup.utilities

>  A collection of generally useful functions. 

### Sub-Module: gitbuilding.buildup.core

>  This submodule contains the main BuildUp Documentation class. 

#### Class: gitbuilding.buildup.core.Documentation

>  This class represents the documentation in a BuildUp project. All other objects representing Pages, Libraries, Parts, Partlists, Links, etc are held within this the Documentation object. The most simple use of the Documentation object is to initialise it with a configuration and then run `buildall` with a list of input files. 

### Sub-Module: gitbuilding.buildup.config

>  This submodule is used to parse the input configuration into an object. The configuration object is a python dataclass created by ConfigSchema which is a marshmallow schema. The data class contains fields defined by ConfigSchema. Subclass ConfigSchema to add fields. 

#### Class: gitbuilding.buildup.config.ConfigSchema

>  This is the schema for the main configuration object that is used in BuildUp. The configuration object is generated by passing a dictionary into this marshmallow schema. A validation error is returned for extra fields. If you want to use the configuration dictionary to hold to add information you can either subclass this schema, or you can use  ConfigSchema.validate() to find extra fields and remove them before the configuration object is created. Extra fields are not allowed to help catch typos the configuration. 

#### Class: gitbuilding.buildup.config._CategoriesField

> CategoriesField is used by marshmallow to serialise and de-serialise     custom_categories. All category names are turned lower case.

#### Class: gitbuilding.buildup.config._CategorySchema

>  Marshmallow schema for validating custom BuildUp categories. On load it converts the input dictionary to a python dataclass. 

#### Class: gitbuilding.buildup.config._DefaultCategory

> DefaultCategory is used by marshmallow to serialise and de-serialise     the DefaultCategories. This object will hopefully be removed once we     can find a better way to compare the field to the de-serialised output     of custom_categories

#### Class: gitbuilding.buildup.config._NavSchema

>  Marshmallow schema for parsing the navigation 

### Sub-Module: gitbuilding.buildup.url

>  The url module deals with URL translation in buildup. URL translation includes: changing urls between relative and absolute; adjusting relative paths when the same link is used in another location; and applying custom modifier functions to the paths. 

#### Class: gitbuilding.buildup.url.URLRules

>  This class holds the rules for translating the url in the BuildUp into the final output url. When initialised the only value set is whether output paths will be relative to the root of the documentation directory. If false they will be output relative to the page they are on (default). The `add_modifier` function can be used to add custom functions that modify the urls further, for example to prepend a baseurl, or to change a file extension. 

#### Class: gitbuilding.buildup.url.URLTranslator

>  Translates the url in the BuildUp into the final output url. This class should only be made by URLRules "create_translator". 