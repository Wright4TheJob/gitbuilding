#### Problem with current syntax

<!-- Give example of of the where the current syntax fails or is unclear.
Or describe something you would like to do where there is no syntax defined -->

...

#### Proposed syntax
<!-- The syntax you would prefer -->

...

#### Benefits of new syntax
<!-- How would this new syntax benefit the project. -->

...

/label ~"Syntax"
