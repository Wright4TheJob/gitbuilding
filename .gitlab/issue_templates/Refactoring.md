<!-- Use this template to suggests changes to the code structure or use of other libraries-->

#### Why does the code need refactoring?
<!-- Difficult to contribute, will save time in the future, more secure, etc-->

...

#### Suggested change
<!-- How do you propose we change the code? Try to be specific, perhaps give examples -->

...

#### How does this solve the problems above
<!-- Give examples where the refactored code solves the problems above-->

...

/label ~"Discussion"
