#### Steps to reproduce
<!-- Please detail the steps needed to reproduce this bug-->
* Run `gitbuilding serve`
* ...

#### What happened?
<!-- Please explain what happened. Console output may help here.-->

...

#### What should have happened?

...

#### Version/OS/Installation method


* **Version**: ...<!-- Add the result from `gitbuilding --version`-->
* **Operating System**: ... <!--e.g. Windows 10-->
* **Installation source** ... <!-- Did you install with `pip install gitbuilding` or did you install from GitLab?-->

<!-- Note if you installed code from GitLab please check the developer install guide before posting an issue.
https://gitlab.com/gitbuilding/gitbuilding/-/blob/master/DeveloperInstallation.md
-->
#### Relevant files or links

If the bug only happens on a certain file or the files in a certain repository provide a link to the repository, or drag and drop the files here.

/label ~"Bug"
