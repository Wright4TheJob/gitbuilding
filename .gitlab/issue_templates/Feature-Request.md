#### Problem to solve
<!-- What problem, either in GitBuilding, or in general hardware documentation/dev does this solve?-->

### Who will benefit
<!-- Is this feature to benefit the user of GitBuilding? Someone reading the Docs? Someone else? -->

### Proposal

<!-- How are we going to solve the problem? -->

#### Version of GitBuilding you use
<!-- Feature requests sometimes are in response to new or deprecated features. The version you are using puts the request in context. -->
* **Version**: ...<!-- Add the result from `gitbuilding --version`-->


#### Further information

...

/label ~"Feature"
