"""
Lists markdown extensions used.
"""
base_extensions = ("markdown.extensions.tables",
                   "markdown.extensions.attr_list",
                   "markdown.extensions.fenced_code",
                   "gitbuilding.buildup.buildup_list:BuildUpListExtension",
                   "markdown.extensions.toc",
                   "markdown.extensions.md_in_html")
