module.exports = {
  outputDir: "../../gitbuilding/static/webapp",
  publicPath: "/static/webapp/",
  pages: {
    buildup_editor: {
      entry: "src/buildup-editor-main.js",
      filename: "buildup-editor.html",
      template: "public/index.html"
    },
    conf_editor: {
      entry: "src/conf-editor-main.js",
      filename: "conf-editor.html",
      template: "public/index.html"
    },
    project_selector: {
      entry: "src/project-selector-main.js",
      filename: "project-selector.html",
      template: "public/index.html"
    }
  }
};
